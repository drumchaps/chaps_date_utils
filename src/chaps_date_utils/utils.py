#-*- coding: utf-8 -*-

import datetime
from datetime import timedelta
from random import randint
import calendar


def first_day(year):
    """ Returns a date object with the first day of a given year
    """
    return datetime.date( day=1, month=1, year=year )
    pass


def last_day(year):
    """Returns a date object with the last day of a given year
    """
    return datetime.date( day=31, month=12, year=year )
    pass


def month_max_day( date ):
    """ Returns the last day of a month 
    """
    return calendar.monthrange(date.year, date.month )[1]
    

def get_epoch():
    """"Returns the epoch start value """
    return datetime.datetime.utcfromtimestamp(0)

def date_to_epoch(dt):
    """Returns the epoch based on a recived datetime"""
    epoch = get_epoch()
    return (dt - epoch ).total_seconds() * 1000.0


def random_day_between( start, end ):
    """Returns a random day between 2 dates.\
    """
    return start + timedelta(
            seconds=randint(0, int((end - start).total_seconds()))
    )


def get_date_between_years( year1, year2 ):
    """Gets a random day for a person having ages between year1 and year2.\
    """
    today = datetime.date.today()
    date1 = datetime.date(year = (today.year - year1) , month = 1, day=1)
    date2 = datetime.date( year = ( today.year - year2), month = 12, day=31)
    return random_day_between( date2, date1 )
