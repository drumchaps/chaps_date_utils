
Script with functions for datetime repetitive tasks.


HOW TO USE:
```python

from chaps_date_utils.utils import *
import datetime

#First day of a year...
print first_day( 1990 )

#Last day
print last_day( 1990 )


#month_max_day
print month_max_day( datetime.datetime( year = 2012, month=2, day=1) )

#get epoch start value
print get_epoch()

#date to epoch
print date_to_epoch( datetime.datetime.now() )


```



