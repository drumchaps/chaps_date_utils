from distutils.core import setup

setup(
    name="chaps_date_utils",
    version="0.0.1",
    author="Chaps",
    author_email="drumchaps@gmail.com",
    maintainer="Chaps",
    maintainer_email="drumchaps@gmail.com",
    url="https://bitbucket.org/drumchaps/chaps_date_utils",
    packages  = [
        "chaps_date_utils",
    ],
    package_dir={'chaps_date_utils': 'src/chaps_date_utils'},
    #install_requires = ["requests",]
)


